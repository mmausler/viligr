from django import forms
from haystack.forms import SearchForm

class ViligrSearchForm(SearchForm):
    
    def search(self):
        sqs = super(ViligrSearchForm, self).search()

        if not self.is_valid():
            return self.no_query_found()

        return sqs
