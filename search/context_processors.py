from .forms import ViligrSearchForm

def search_form(request):
    return {
        'search_form': ViligrSearchForm()
    }
