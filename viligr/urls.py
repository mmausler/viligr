from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login, logout
from filebrowser.sites import site

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from accounts.views import LoginView
#from feeds.views import HomeView
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'viligr.views.home', name='home'),
    # url(r'^viligr/', include('viligr.foo.urls')),

    url(r'^login/$', LoginView.as_view(), name="login"),
    url(r'^logout/$', logout, {'template_name': 'registration/logout.html', 'next_page': '/login/' }, name="my_logout"),

    url(r'^$', LoginView.as_view()),
    #Social Auth
    url(r'', include('social_auth.urls')),
url(r'^tinymce/', include('tinymce.urls')),
    url(r'^account/', include('accounts.urls')),
    url(r'^reader/', include('feeds.urls')),
    url(r'^api/', include('api.urls', namespace='api')),
    url(r'^publish/', include('grappelli.urls')),
    url(r'^search/', include('haystack.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
    #url(r'', include('registration.backends.default.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/filebrowser/', include(site.urls)),
)
