from django.contrib.auth.models import ( AbstractBaseUser, BaseUserManager, PermissionsMixin )
from django.db import models
from django.core.urlresolvers import reverse
from feeds.models import Article, Feed

class UserProfileManager(BaseUserManager):
    def create_user(self, email, first_name=None, last_name=None, zip_code=None, password=None):

        if not email:
            msg = "Users must have an email address"
            raise ValueError(msg)

        user = self.model(
                email=UserProfileManager.normalize_email(email),
                first_name=first_name,
                last_name=last_name,
                zip_code=zip_code,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):

        user = self.create_user(email, password=password)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

    def get_saved_articles(self, user):
        return self.filter(saved_articles__userprofile_id__exact=self.user.id)

class UserProfile(AbstractBaseUser, PermissionsMixin):
        email = models.EmailField(
                verbose_name='email address',
                max_length=255,
                unique=True,
                db_index=True,
        )
        first_name = models.CharField(max_length=200, blank=True, null=True)
        last_name = models.CharField(max_length=200, blank=True, null=True)
        zip_code = models.CharField(max_length=5, blank=True, null=True)
        is_active = models.BooleanField(default=True)
        is_admin = models.BooleanField(default=False)
        is_staff = models.BooleanField(default=False)

        objects = UserProfileManager()

        USERNAME_FIELD = 'email'

        subscribed_feeds = models.ManyToManyField(Feed, blank=True, null=True)
        liked_articles = models.ManyToManyField(Article, related_name='liked', blank=True, null=True)
        saved_articles = models.ManyToManyField(Article, related_name='user_saved', blank=True, null=True)

        def __unicode__(self):
            return self.email
        
        def get_full_name(self):
            return "%s %s" % (self.first_name, self.last_name)

        def get_short_name(self):
            return self.email

        def has_perm(self, perm, obj=None):
            return True

        def has_module_perms(self, app_label):
            return True

