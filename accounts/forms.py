from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import UserProfile
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit

class UserProfileCreateForm(forms.ModelForm):

    confirm_email = forms.EmailField("Confirm email!", required=True,)
    confirm_password = forms.CharField(widget=forms.PasswordInput())
    password = forms.CharField(label="Password", widget=forms.PasswordInput())
    
    class Meta:
        model = UserProfile
        fields = ['email','password','first_name','last_name','zip_code']

    def __init__(self, *args, **kwargs):

        super(UserProfileCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.form_action = ''
        self.helper.layout = Layout(
            Fieldset(
                'legend',
                'email',
                'confirm_email',
                'first_name',
                'last_name',
                'zip_code',
                'password',
                'confirm_password'
            )
        )

        self.helper.add_input(Submit('save', 'save'))
            
        return super(UserProfileCreateForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        user = super(UserProfileCreateForm, self).save(commit=False)
        user.set_password(self.cleaned_data.get('password'))
        if commit:
          user.save()
        return user

    def clean(self, *args, **kwargs):

        if (self.cleaned_data.get('email') !=
            self.cleaned_data.get('confirm_email')):

            raise ValidationError(
                "Email addresses must match!"
            )

        if (self.cleaned_data.get('password') !=
            self.cleaned_data.get('confirm_password')):

            raise ValidationError(
                "Passwords do not match."
            )

        return self.cleaned_data

class UserProfileChangeForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['first_name','last_name','zip_code']

    def __init__(self, *args, **kwargs):
        userprofile = kwargs.pop('user', None)
        super(UserProfileChangeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.form_action = ''
        self.helper.layout = Layout(
            Fieldset(
                'legend',
                'first_name',
                'last_name',
                'zip_code',
            )
        )

        self.helper.add_input(Submit('save', 'save'))
        return super(UserProfileChangeForm, self).__init__(*args, **kwargs)

class LoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)
