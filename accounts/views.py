import urlparse
from django.contrib import messages
from django.conf import settings
from django.contrib.auth import (
            REDIRECT_FIELD_NAME, login, logout, authenticate
            )
from django.views.generic import View, CreateView, UpdateView, DetailView, ListView
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse_lazy, reverse
import forms
from django.http import HttpResponse, HttpResponseRedirect

from braces.views import LoginRequiredMixin, UserFormKwargsMixin

from .models import UserProfile
from feeds.models import Article

class UserProfileActionMixin(object):

    @property
    def action(self):
        msg = "{0} is missing action.".format(self.__class__)
        raise NotImplementedError(msg)

    def form_valid(self, form):
        msg = "User {0}!".format(self.action)
        messages.info(self.request, msg)
                
        return super(UserProfileActionMixin, self).form_valid(form)

class UserProfileCreateView(UserProfileActionMixin, CreateView):
    model = UserProfile
    success_url = reverse_lazy('profile')
    form_class = forms.UserProfileCreateForm
    action = "created"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse_lazy('profile'))
        else: 
            return super(UserProfileCreateView, self).dispatch(request, *args, **kwargs)

class UserProfileUpdateView(LoginRequiredMixin, UserFormKwargsMixin, UserProfileActionMixin, UpdateView):
    model = UserProfile
    success_url = reverse_lazy('profile')
    form_class = forms.UserProfileChangeForm
    action = "updated"

    def get_object(self):
        return  self.request.user

    def form_invalid(self, form): 
        msg = "bad form"
        messages.info(self.request, msg)

        return super(UserProfileUpdateView, self).form_invalid(form)

class UserProfileDetailView(LoginRequiredMixin, DetailView):
    model = UserProfile
    
    def get_object(self):
        return  self.request.user

    def get_context_data(self, **kwargs):
        kwargs['userprofile'] = self.object
        kwargs['pk'] = self.object.pk
        context = super(UserProfileDetailView, self).get_context_data(**kwargs)
        return context

class UserProfileSaveArticleView(LoginRequiredMixin, View):
    model = UserProfile

    def get(self, request, **kwargs):
        article_id = self.kwargs["article_id"]
        user = self.request.user
        article = Article.objects.get(id=article_id)
        user.save()
        user.saved_articles.add(article)
        return HttpResponse('')

class LoginView(FormView):
    form_class = forms.LoginForm
    redirect_field_name = REDIRECT_FIELD_NAME
    template_name = 'login.html'
    success_url = '/account'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(reverse_lazy('profile'))
        else: 
            return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']

        user = authenticate(email=email, password=password)

        if user is not None:
            if user.is_active:
                login(self.request, user)
                return HttpResponseRedirect(self.get_success_url())
        else:        
            return self.form_invalid()
    
    def form_invalid(self):
        return HttpResponseRedirect(reverse('login'))
    
    def get_success_url(self):
        if self.success_url:
            redirect_to = self.success_url
        else:
            redirect_to = self.request.REQUEST.get(self.redirect_field_name, '')

        netloc = urlparse.urlparse(redirect_to)[1]
        if not redirect_to:
            redirect_to = settings.LOGIN_REDIRECT_URL
        elif netloc and netloc != self.request.get_host():
            redirect_to = settings.LOGIN_REDIRECT_URL
        return redirect_to
      
    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:                
            return self.form_invalid()
