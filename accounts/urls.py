from django.conf.urls import patterns
from django.conf.urls import *
from . import views


urlpatterns = patterns('',
        url(r'^$', views.UserProfileDetailView.as_view(),
            name='profile',),
        url(r'^create', views.UserProfileCreateView.as_view(),
            name='userprofile_create',),
        url(r'^update/$', views.UserProfileUpdateView.as_view(),
            name='userprofile_update',),
        url(r'^save_article/(?P<article_id>\d+)', 
            views.UserProfileSaveArticleView.as_view(), name='save_article'),
)
