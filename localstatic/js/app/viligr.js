Viligr = Ember.Application.create({
    rootElement: '#articles'
});

Viligr.Article = DS.Model.extend({
    title: DS.attr('string'),
    description: DS.attr('string'),
    author: DS.attr('string')
});


/*
Viligr.Store = DS.DjangoRESTStore.extend({
          adapter: DS.DjangoRESTAdapter.create({
              namespace: 'api'
          })
});
*/
Viligr.Store = DS.Store.extend({
          adapter: DS.DjangoRESTAdapter.create({
              namespace: 'api'
          })
});

Viligr.Router.map(function () {
      this.resource('articles', { path : '/' });
});

Viligr.ArticlesController = Ember.ArrayController.extend({
  page: 1,
  perPage: 10,
  totalPages: (function() {
    return Math.ceil(this.get('length') / this.get('perPage'));
  }).property('length', 'perPage'),
  
  pages: (function() {
    var collection = Ember.A();
    
    for(var i = 0; i < this.get('totalPages'); i++) {
      collection.pushObject(Ember.Object.create({
        number: i + 1
      }));
    }
    
    return collection;      
  }).property('totalPages'),
  
  hasPages: (function() {
    return this.get('totalPages') > 1;
  }).property('totalPages'),
  
  prevPage: (function() {
    var page = this.get('page');
    var totalPages = this.get('totalPages');
    
    if(page > 1 && totalPages > 1) {
      return page - 1;
    } else {
      return null;
    }
  }).property('page', 'totalPages'),
  
  nextPage: (function() {
    var page = this.get('page');
    var totalPages = this.get('totalPages');
    
    if(page < totalPages && totalPages > 1) {
      return page + 1;
    } else {
      return null;
    }
  }).property('page', 'totalPages'),
 
  
  paginatedContent: (function() {
    var start = (this.get('page') - 1) * this.get('perPage');
    var end = start + this.get('perPage');
    
    return this.get('arrangedContent').slice(start, end);
  }).property('page', 'totalPages', 'arrangedContent.[]'),
  
  selectPage: function(number) {
    this.set('page', number);
  },
  
  toggleOrder: function() {
    this.toggleProperty('sortAscending');
  }
});

Viligr.ArticlesRoute = Ember.Route.extend({
    model: function() {
        return Viligr.Article.find();
    },
  page: 1,
  perPage: 10,
  totalPages: (function() {
    return Math.ceil(this.get('length') / this.get('perPage'));
  }).property('length', 'perPage'),
  
  pages: (function() {
    var collection = Ember.A();
    
    for(var i = 0; i < this.get('totalPages'); i++) {
      collection.pushObject(Ember.Object.create({
        number: i + 1
      }));
    }
    
    return collection;      
  }).property('totalPages'),
  
  hasPages: (function() {
    return this.get('totalPages') > 1;
  }).property('totalPages'),
  
  prevPage: (function() {
    var page = this.get('page');
    var totalPages = this.get('totalPages');
    
    if(page > 1 && totalPages > 1) {
      return page - 1;
    } else {
      return null;
    }
  }).property('page', 'totalPages'),
  
  nextPage: (function() {
    var page = this.get('page');
    var totalPages = this.get('totalPages');
    
    if(page < totalPages && totalPages > 1) {
      return page + 1;
    } else {
      return null;
    }
  }).property('page', 'totalPages'),
 
  
  paginatedContent: (function() {
    var start = (this.get('page') - 1) * this.get('perPage');
    var end = start + this.get('perPage');
    
    return this.get('arrangedContent').slice(start, end);
  }).property('page', 'totalPages', 'arrangedContent.[]'),
  
  selectPage: function(number) {
    this.set('page', number);
  },
  
  toggleOrder: function() {
    this.toggleProperty('sortAscending');
  }
});

Viligr.PageController = Ember.ObjectController.extend({
  currentPage: Ember.computed.alias('parentController.page'),
  
  active: (function() {
    return this.get('number') === this.get('currentPage');
  }).property('number', 'currentPage')
});
//console.log(Viligr.Article.find(1));
