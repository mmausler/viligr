        $('.article .title').click(function(){
            //var id = $(this).closest("div").attr("id");
            var url = $(this).attr('data-url');
            var article_id = $(this).closest('.article').attr('id');
            console.log(url);
            $('#articles').fadeOut();
            var social_buttons =$(this).closest('.article').find('.social-buttons');
            social_buttons.css('float','left');
            $('#reader-pane').prepend(social_buttons);
            $('#reader-pane iframe').attr('src', '/reader/'+article_id);

            $("#readerframe").load(function() {
                $(this).height( $(this).contents().find("body").height() + 80 );
            });

            $('#reader-pane').fadeIn();
            $('.pagination').hide();
            //$('#' +id + ' .meta').toggle();
        });
        $('.close').click(function() {
            $('#reader-pane').fadeOut();
            $('#articles').fadeIn();
            $('.pagination').show();
        });
        $('.save').click(function(){
                    console.log("clicked");
            var article_id = $(this).closest('.article').attr('id');
            $.ajax({
                type: "GET",
                url: "/account/save_article/" + article_id,
                dataType: "html",
                success: function() {
                    console.log("saved article");
                }
            });
        });
        $('.like').click(function(){
            var article_id = $(this).closest('.article').attr('id');
            $.ajax({
                type: "GET",
                url: "/reader/like_article/" + article_id,
                dataType: "html",
                success: function() {
                    console.log("liked article");
                }
            });
            $(this).addClass('btn-primary');
        });

