$(".description-toggle").click(function() {
    var description = $(this).closest('.feed').find('.feed-description');
    description.slideToggle();
    if($(this).hasClass('open')){
        $(this).html('See More');
        $(this).removeClass('open');
    } else {
        $(this).addClass('open');
        $(this).html('See Less');
    }
});
$('.add-btn').click(function() {
    if($(this).hasClass('added')){
        $(this).removeClass('added');
        $(this).closest('.checkbox').find('input').prop('checked', true);
    } else {
        $(this).addClass('added');
        $(this).closest('.checkbox').find('input').prop('checked', false);
    }
});

