import datetime
from haystack import indexes
from .models import Article, Feed

class ArticleIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    author = indexes.CharField(model_attr='author')
    pubdate = indexes.DateTimeField(model_attr='pubdate')
    tags = indexes.MultiValueField(indexed=True, stored=True)

    def get_model(self):
        return Article

class FeedIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    tags = indexes.MultiValueField(indexed=True, stored=True)

    def get_model(self):
        return Feed

