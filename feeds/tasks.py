from celery.task import task

from dynamic_scraper.utils.task_utils import TaskUtils
from .models import Feed, Article

@task()
def run_spiders():
    t = TaskUtils()
    t.run_spiders(Feed, 'scraper', 'scraper_runtime', 'article_spider')

@task()
def run_checkers():
    t = TaskUtils()
    t.run_checkers(Article, 'feed__scraper', 'checker_runtime', 'article_checker')
