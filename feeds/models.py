from django.db import models
from django.conf import settings
from dynamic_scraper.models import Scraper, SchedulerRuntime
from scrapy.contrib.djangoitem import DjangoItem
from django.dispatch import receiver
from taggit.managers import TaggableManager
from django.template.defaultfilters import slugify

class Feed(models.Model):
    name = models.CharField(max_length=200)
    url = models.URLField()
    icon = models.URLField(blank=True, null=True)
    description = models.TextField(blank=True)
    scraper = models.ForeignKey(Scraper, blank=True, null=True, on_delete=models.SET_NULL)
    scraper_runtime = models.ForeignKey(SchedulerRuntime, blank=True, null=True, on_delete=models.SET_NULL)
    tags = TaggableManager(blank=True)
    slug = models.SlugField()
    category = models.ForeignKey('categories.Category', blank=True, null=True)

    def __unicode__(self):
            return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)

        super(Feed, self).save(*args, **kwargs)


class Article(models.Model):
    class Meta:
        ordering = ['-pubdate']

    title = models.CharField(max_length=300)
    feed = models.ForeignKey(Feed)
    description = models.TextField(blank=True)
    url = models.URLField()
    pubdate = models.DateTimeField()
    author = models.CharField(max_length=200, blank=True)
    checker_runtime = models.ForeignKey(SchedulerRuntime, blank=True, null=True, on_delete=models.SET_NULL)
    tags = TaggableManager()
    slug = models.SlugField(max_length=200)

    def __unicode__(self):
            return self.title
                
    def save(self, *args, **kwargs):
        if not self.id:
            trimmed_title = ' '.join(self.title.split()[:5])
            self.slug = slugify(trimmed_title)
            
        super(Article, self).save(*args, **kwargs)


class ArticleItem(DjangoItem):
    django_model = Article

class List(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)
    feeds = models.ManyToManyField(Feed)
    category = models.ForeignKey('categories.Category', blank=True, null=True)

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return "/reader/list/%i" % self.id

@receiver(models.signals.pre_delete)
def pre_delete_handler(sender, instance, using, **kwargs):
    if isinstance(instance, Article):
      if instance.checker_runtime:
        instance.checker_runtime.delete()

models.signals.pre_delete.connect(pre_delete_handler)
