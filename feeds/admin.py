from django.contrib import admin
from .models import Feed, Article, List

class ListAdmin(admin.ModelAdmin):
    list_display = ("name", "owner", "feed", "category")

class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Feed)
admin.site.register(Article, ArticleAdmin)
admin.site.register(List)
