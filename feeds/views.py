# Create your views here.
from django.contrib import messages
from django.views.generic import View, CreateView, UpdateView, DetailView, ListView, DeleteView
from django.http import HttpResponse, HttpResponseRedirect
import forms
from braces.views import LoginRequiredMixin, UserFormKwargsMixin, SetHeadlineMixin
from .models import Feed, Article, List
from forms import ListCreateUpdateForm
from django.core.urlresolvers import reverse, reverse_lazy

class FeedsActionMixin(object):

    @property
    def action(self):
        msg = "{0} is missing action.".format(self.__class__)
        raise NotImplementedError(msg)

    def form_valid(self, form):
        msg = "{0}!".format(self.action)
        messages.info(self.request, msg)
        return super(FeedsActionMixin, self).form_valid(form)

#class ListCreateView(LoginRequiredMixin, UserFormKwargsMixin, FeedsActionMixin, CreateView):
#    model = List
#    form_class = forms.ListCreateForm
#    action = "List created"
#
#    def get_context_data(self, **kwargs):
#        context = super(ListCreateView, self).get_context_data(**kwargs)
#        context['form'] = forms.ListCreateForm()
#        return context

class ListCreateView(LoginRequiredMixin, UserFormKwargsMixin, FeedsActionMixin, CreateView):
    model = List
    form_class = forms.ListCreateUpdateForm
    #template_name = 'list_create.html'
    action = "List created"

    def get_context_data(self, **kwargs):
        context = super(ListCreateView, self).get_context_data(**kwargs)
        context['form'] = forms.ListCreateUpdateForm()
        return context

class ListUpdateView(LoginRequiredMixin, UserFormKwargsMixin, FeedsActionMixin, UpdateView):
    model = List
    form_class = forms.ListCreateUpdateForm
    action = "updated"

    def get_context_data(self, **kwargs):
        context = super(ListUpdateView, self).get_context_data(**kwargs)
        context['form'] = forms.ListCreateUpdateForm()
        return context

class ListDetailView(SetHeadlineMixin, DetailView):
    model = List
    
    def get_headline(self, **kwargs):
        return self.object.name

    def get_context_data(self, **kwargs):
        kwargs['pk'] = self.object.pk
        #feeds = Feed.objects.get
        context = super(ListDetailView, self).get_context_data(**kwargs)
        context['article_list'] = Article.objects.filter(feed__id__in=self.object.feed.all())
        return context

class ListDeleteView(LoginRequiredMixin, DeleteView):
    model = List
    success_url = reverse_lazy('article_list')

class ArticleDetailView(SetHeadlineMixin, DetailView):
    model = Article

    def get_headline(self, **kwargs):
        return self.object.title

class ArticleListView(LoginRequiredMixin, SetHeadlineMixin, ListView):
    model = Article
    context_object_name = "articles"
    paginate_by = 10

    def get_headline(self, **kwargs):
        if (self.kwargs['filter'] == 'feed'):
            feed = Feed.objects.get(id=self.kwargs['feed_id'])
            return feed.name
        elif (self.kwargs['filter'] == 'list'):
            my_list = List.objects.get(id=self.kwargs['list_id'])
            return my_list.name
        else:
            return "Articles"


    def get_queryset(self, **kwargs):
        queryset_filters = {
            #'feed__id' : self.kwargs['feed_id'],
        }

        if (self.kwargs['filter'] == 'feed'):
            return Article.objects.filter(feed__id=self.kwargs['feed_id'])
        elif (self.kwargs['filter'] == 'list'):
            my_list = List.objects.get(id=self.kwargs['list_id'])
            return Article.objects.filter(feed__id__in=my_list.feeds.all())
        else:
            return Article.objects.all()

    def get_context_data(self, **kwargs):
        context = super(ArticleListView, self).get_context_data(**kwargs)

        if (self.kwargs['filter'] == 'list'):
            context['list'] = List.objects.get(id=self.kwargs['list_id'])

        return context

class ArticleLikeView(LoginRequiredMixin, View):
    model = Article

    def get(self, request, **kwargs):
        article_id = self.kwargs["article_id"]
        user = self.request.user
        article = Article.objects.get(id=article_id)
        article.save()
        article.liked_by.add(user)
        return HttpResponse('')

class SavedArticlesView(LoginRequiredMixin, SetHeadlineMixin, ListView):
    model = Article
    headline = "Saved Articles"

    def get_context_data(self, **kwargs):
        kwargs['articles'] = Article.objects.filter(user_saved__id=self.request.user.id)
        context = super(SavedArticlesView, self).get_context_data(**kwargs)
        return context

class FeedSubscribeView(LoginRequiredMixin, View):
    model = Feed
