import os

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "viligr.settings.local")

BOT_NAME = 'feeds'

SPIDER_MODULES = ['dynamic_scraper.spiders', 'feeds.scraper',]
USER_AGENT = '%s/%s' % (BOT_NAME, '1.0')

ITEM_PIPELINES = [
        'dynamic_scraper.pipelines.ValidationPipeline',
        'feeds.scraper.pipelines.DjangoWriterPipeline',
]

