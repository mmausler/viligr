from django import forms
from .models import List,Feed
from django.core.exceptions import ValidationError
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Field, Layout, Fieldset, ButtonHolder, Submit, Div, HTML
import floppyforms as forms
from braces.forms import UserKwargModelFormMixin

class FeedListWidget(forms.CheckboxSelectMultiple):
    template_name = 'feeds/feed_select.html'
    class Media:
        css = {
            'all': ('css/feed_list_style.css',)
            }
        js = ('js/feed_list.js')

    def __init__(self, attrs={}):
        super(FeedListWidget, self).__init__(attrs)


    def render(self, name, value, attrs=None, choices=()):
        feeds = Feed.objects.all()
        classes = tuple([(c.id, c.name, c.description) for c in feeds])
        self.choices = classes
        output = super(FeedListWidget, self).render(name, values, attrs, choices)
        return output

class ListCreateUpdateForm(UserKwargModelFormMixin, forms.ModelForm):

    class Meta:
        model = List
        fields = ['name','feeds']
        widgets = {
                'name': forms.TextInput,
                'feeds': FeedListWidget,
                }

    feeds = forms.ModelMultipleChoiceField(queryset=Feed.objects.all(), widget=FeedListWidget())

    def __init__(self, *args, **kwargs):
        super(ListCreateUpdateForm, self).__init__(*args, **kwargs)
        
        owner = self.user
        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
            Fieldset(
                '',
                HTML("""
                    <p>Create a List here</p>
                """),
                'name',
            ),
            Field('feeds', template="feeds/feeds_select.html"),
        )

        self.helper.add_input(Submit('save', 'save'))
        
        return super(ListCreateUpdateForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        list = super(ListCreateUpdateForm, self).save(commit=False)
        list.owner = self.user

        if commit:
            list.save()
            self.save_m2m()
        return list

class FeedSubscribeForm(UserKwargModelFormMixin, forms.ModelForm):
    class Meta:
        model = Feed
        fields = ['name','description']

    def __init__(self,*args, **kwargs):
        super(FeedSubscribeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_method = 'POST'
        self.helper.layout = Layout(
                Fieldset(
                    'name',
                    'feeds',
        )
        )

        self.helper.add_input(Submit('save','save'))

        return super(FeedSubscribeForm, self).__init__(*args, **kwargs)
