from .models import List

def user_lists(request):
    return { 'user_lists' : List.objects.filter(owner__id=request.user.id)}
