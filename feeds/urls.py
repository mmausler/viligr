from django.conf.urls import patterns
from django.conf.urls import *
from django.template.defaultfilters import slugify

from . import views

urlpatterns = patterns('',
        url(r'^feed/subscribe', views.FeedSubscribeView.as_view(),
            name='feed_subscribe',),
        url(r'^list/create', views.ListCreateView.as_view(),
            name='list_create',),
        url(r'^list/update/(?P<pk>\d+)/$', views.ListUpdateView.as_view(),
            name='list_update',),
        url(r'^list/delete/(?P<pk>\d+)/$', views.ListDeleteView.as_view(),
            name='list_delete',),
        url(r'^like_article/(?P<article_id>\d+)/$', views.ArticleLikeView.as_view(),
            name='like_article',),
        url(r'^saved-articles/$', views.SavedArticlesView.as_view(), 
            name='saved_articles'),
        url(r'^(?P<pk>\d+)/$', views.ArticleDetailView.as_view(),
            name='article_detail',),
        url(r'^$', views.ArticleListView.as_view(),
            {'filter':'all'},
            name='article_list',),
        url(r'^feed/(?P<feed_id>\d+)/$', views.ArticleListView.as_view(),
            {'filter':'feed'},
            name='feed_view'),
        url(r'^list/(?P<list_id>\d+)/$', views.ArticleListView.as_view(),
            {'filter':'list'},
            name='list_detail',),
)
